﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Threading.Tasks;
using Common;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using TinTuc.Service.Se;
using TinTuc.Service.System;
using TinTuc.WebApplication.Areas.Se.Models;

namespace TinTuc.WebApplication.Areas.Se.Controllers
{
    [Area("Se")]
    public class HomeController : Controller
    {
        private readonly ILogger<HomeController> _logger;
        private readonly ICategoryPublicService _categoryPublicService;
        private readonly IArticlePublicService _articlePublicService;
        public HomeController(IArticlePublicService articlePublicService, ICategoryPublicService categoryPublicService, ILogger<HomeController> logger)
        {
            _logger = logger;
            _categoryPublicService = categoryPublicService;
            _articlePublicService = articlePublicService;
        }
        /// <summary>
        /// Get List All Article Of Category Differences
        /// </summary>
        /// <returns>View</returns>
        public IActionResult Index()
        {
            Guid idCateCovid =  new Guid("21c596f1-525c-4cb3-b448-151b8ea03d08");
            Guid idCateTech =  new Guid("32482be0-7ed6-4c25-b882-d0755c3333dd");
            Guid idCateTravel = new Guid("64d0aa1c-39e5-4f6a-925a-fbd9f9d2c11d");
            Guid idCateLaw =  new Guid("32482be0-7ed6-4c25-b882-d0755c3333dd");
            Guid idCateSociety =  new Guid("79fd7b72-b943-4ecd-8d7a-4131b9e86ac3");
            ViewBag.ArticleCovid = _articlePublicService.GetArticleViewModelForCategory(idCateCovid);
            ViewBag.ArticleTech = _articlePublicService.GetArticleViewModelForCategory(idCateTech);
            ViewBag.ArticleLaw = _articlePublicService.GetArticleViewModelForCategory(idCateLaw);
            ViewBag.ArticleTravel = _articlePublicService.GetArticleViewModelForCategory(idCateTravel);
            ViewBag.ArticleidSociety = _articlePublicService.GetArticleViewModelForCategory(idCateSociety);
            var AllArticleRandom = _articlePublicService.GetAllRandom();
            var AllArticle = _articlePublicService.GetAll();
            ViewBag.AllArticleRandom = AllArticleRandom;
            ViewBag.AllArticle = AllArticle;
            ViewBag.AllArticleRandom4 = AllArticleRandom.Models.ToList().Skip(1).Take(4);
            return View();
        }
        [ResponseCache(Duration = 0, Location = ResponseCacheLocation.None, NoStore = true)]
        public IActionResult Error()
        {
            return View(new ErrorViewModel { RequestId = Activity.Current?.Id ?? HttpContext.TraceIdentifier });
        }
        public PartialViewResult Header()
        {
            var lstCategory = _categoryPublicService.GetAll();
            ViewBag.lstCategory = lstCategory;
            return PartialView();
        }
        public PartialViewResult Slider()
        {
            return PartialView();
        }
        public PartialViewResult Footer()
        {
            return PartialView();
        }
    }
}