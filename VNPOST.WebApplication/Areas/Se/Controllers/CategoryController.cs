﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using TinTuc.Service.Se;

namespace TinTuc.WebApplication.Areas.Se.Controllers
{
    [Area("Se")]
    public class CategoryController : Controller
    {
        private readonly ICategoryPublicService _categoryPublicService;
        private readonly IArticlePublicService _articlePublicService;

        public CategoryController(ICategoryPublicService categoryPublicService, IArticlePublicService articlePublicService) 
        {
            _categoryPublicService = categoryPublicService;
            _articlePublicService = articlePublicService;
        }
        public IActionResult Index()
        {
            return View();
        }
        public IActionResult GetCategory(Guid id)
        {
            ViewBag.ArticleForCategory = _categoryPublicService.GetArticleById(id);
            ViewBag.ArticleNearest = _articlePublicService.GetAll();
            ViewBag.AllCategory = _categoryPublicService.GetAllCategory();
            return View();
        }
    }
}