﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using TinTuc.Service.Se;

namespace TinTuc.WebApplication.Areas.Se.Controllers
{
    [Area("Se")]
    public class ArticleController : Controller
    {
        private readonly IArticlePublicService _articlePublicService;
        public ArticleController(IArticlePublicService articlePublicService)
        {
            _articlePublicService = articlePublicService;
        }
        public IActionResult Index()
        {
            return View();
        }
        public IActionResult Detail(Guid id)
        {
            var article = _articlePublicService.GetById(id);
            var idCate = article.Model.Category.Id;
            var listArticle = _articlePublicService.GetArticleForCategory(idCate);
            ViewBag.articleDetail = article;
            ViewBag.listArticle = listArticle;
            return View();
        }
    }
}