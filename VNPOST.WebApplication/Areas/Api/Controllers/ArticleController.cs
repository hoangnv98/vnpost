﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using TinTuc.Service.Models.Response;
using TinTuc.Service.Se;
using TinTuc.Service.System;
using TinTuc.Service.ViewModels;

namespace TinTuc.WebApplication.Areas.Api.Controllers
{
	[Route("api/article")]
	[ApiController]
	[AllowAnonymous]
	public class ArticleController : ControllerBase
	{
		private readonly IArticlePublicService  _articlePublicService;
		public ArticleController(IArticlePublicService articlePublicService)
		{
			_articlePublicService = articlePublicService;
		}
		[HttpGet("GetAll")]
		public GetResponse<ArticlePublicViewModelTemp> Get()
		{
			return _articlePublicService.GetAllTemp();
		}
		[HttpGet("Get/{number}")]
		public GetResponse<ArticlePublicViewModelTemp> Get(int number)
		{
			return _articlePublicService.GetWithNumber(number);
		}
		[HttpGet("GetById/{id}")]
		public GetResponse<ArticlePublicViewModelTemp> GetbyId(Guid id)
		{
			return _articlePublicService.GetArticleById(id);
		}
	}
}
