﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using TinTuc.Data.Entities;
using TinTuc.Service.Models.Response;
using TinTuc.Service.System;

namespace TinTuc.WebApplication.Areas.Api.Controllers
{
    [Route("api/Category")]
    [ApiController]
    public class CategoryController : ControllerBase
    {
        private readonly ICategoryService _CategoryService;

        public CategoryController(ICategoryService categoryService)
        {
            _CategoryService = categoryService;
        }
        [HttpGet("Get")]
        public GetResponse<Category> Get()
        {
          return _CategoryService.GetAllCategory();
        }
    }
}