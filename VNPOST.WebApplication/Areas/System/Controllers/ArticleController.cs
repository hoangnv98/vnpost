﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using TinTuc.Data.Entities;
using TinTuc.Service.Models;
using TinTuc.Service.Models.Request;
using TinTuc.Service.Models.Response;
using TinTuc.Service.System;
using TinTuc.Service.ViewModels;

namespace TinTuc.WebApplication.Areas.System.Controllers
{
    [Area("System")]
    public class ArticleController : Controller
    {
        private readonly IArticleService _articleService;
        private readonly ICategoryService _categoryService;
        public ArticleController(IArticleService articleService, ICategoryService categoryService)
        {
            _articleService = articleService;
            _categoryService = categoryService;
        }
        public IActionResult Index()
        {
            return View();
        }
        public DeleteResponse Delete(Guid id)
        {
            return _articleService.Delete(id);
        }
        [HttpGet]
        public GetResponse<Article> GetAll()
        {
            return _articleService.GetAll();
        }
        [HttpGet]
        public IActionResult Create()
        {
            //var lstCategory = _categoryService.GetAllCateShow();
            //ViewData["lstCategory"] = lstCategory;
            return View();
        }
        [HttpGet]
        public IActionResult Edit(Guid id)
        {
            var _article  = _articleService.GetById(id);
            var _category = _categoryService.GetAllCateShow();
            ViewBag.article = _article;
            ViewBag.category = _category;
            return View();
        }
        [HttpGet]
        public GetResponse<Article> GetById(Guid id)
        {
            return _articleService.GetById(id);
        }
        [HttpPost]
        public UpdateResponse CreateOrUpdate(UpdateRequest<ArticleViewModel> request)
        {
            _articleService.CreateOrUpdate(request);
            return new UpdateResponse
            {
                Id = request.Model.Id,
                ResponseMessage = "Thành công",
                Status = ResponseStatus.Success
            };
        }
    }
}