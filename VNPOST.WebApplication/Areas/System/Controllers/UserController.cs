﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using TinTuc.Data.Entities.System;
using TinTuc.Service.Models;
using TinTuc.Service.Models.Request;
using TinTuc.Service.Models.Response;
using TinTuc.Service.System;

namespace TinTuc.WebApplication.Areas.System.Controllers
{
    [Area("System")]
    public class UserController : Controller
    {
        private readonly IUserService _userService;
        public UserController(IUserService userService)
        {
            _userService = userService;
        }
        public IActionResult Index()
        {
            return View();
        }
        [HttpGet]
        public IActionResult Create()
        {
            return View();
        }
        public GetResponse<User> GetAll()
        {
            return _userService.GetAll();
        }
        [HttpPost]
        public UpdateResponse CreateOrUpdate(UpdateRequest<User> request)
        {
            _userService.CreateOrUpdate(request);
            return new UpdateResponse
            {
                Id = request.Model.Id,
                ResponseMessage = "Thành công",
                Status = ResponseStatus.Success
            };
        }
        [HttpGet]
        public GetResponse<User> GetUserById(Guid Id)
        {
            return _userService.GetById(Id);
        }
    }
}