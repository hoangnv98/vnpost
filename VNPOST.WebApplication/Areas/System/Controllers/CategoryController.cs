﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using TinTuc.Data.Entities;
using TinTuc.Service.Models;
using TinTuc.Service.Models.Request;
using TinTuc.Service.Models.Response;
using TinTuc.Service.System;
using TinTuc.Service.ViewModels;

namespace TinTuc.WebApplication.Areas.System.Controllers
{
	[Area("System")]
	public class CategoryController : Controller
	{
		private readonly ICategoryService _CategoryService;
		public CategoryController(ICategoryService CategoryService)
		{
			_CategoryService = CategoryService;
		}
		public IActionResult Index()
		{
			return View();
		}
		public GetResponse<Category> GetAll()
		{
			return _CategoryService.GetAllCategory();
		}
		public GetResponse<Category> GetAllCateShow()
		{
			return _CategoryService.GetAllCateShow();
		}
		[HttpGet]
		public GetResponse<Category> GetCateById(Guid Id)
		{
			return _CategoryService.GetById(Id);
		}
		[HttpGet]
		public IActionResult Create()
		{
			return View();
		}
		public IActionResult Detail()
		{
			return View();
		}
		
		[HttpPost]
		public UpdateResponse CreateOrUpdate(UpdateRequest<Category> request)
		{
			_CategoryService.CreateOrUpdate(request);
			return new UpdateResponse
			{
				Id = request.Model.Id,
				ResponseMessage = "Thành công",
				Status = ResponseStatus.Success
			};
		}
		public DeleteResponse Delete(Guid id)
		{
			 return _CategoryService.Delete(id);
		}

	}
}