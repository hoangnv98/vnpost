﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authentication;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Filters;
using TinTuc.Data.Entities.System;
using TinTuc.Service.System;
using TinTuc.Service.ViewModels;

namespace TinTuc.WebApplication.Areas.System.Controllers
{
	[Area("System")]

	public class LoginController : Controller
	{
		private readonly UserManager<User> _userManager;
		private readonly SignInManager<User> _signInManager;
		private readonly IUserService _userService;

		public LoginController(UserManager<User> userManager, SignInManager<User> signInManager, IUserService userService)
		{
			_userManager = userManager;
			_signInManager = signInManager;
			_userService = userService;
		}
        public IActionResult Index()
        {
            return View();
        }
        public IActionResult SignIn()
        {
            return View();
        }
        //[AllowAnonymous]
        [HttpPost]
        public async Task<IActionResult> Login(LoginViewModel model)
        {
            var result = await _signInManager.PasswordSignInAsync(
                model.Username, model.Password, true, false);
            if (result.Succeeded)
            {
                TempData["success"] = "Đăng nhập thành công!";
                return RedirectToAction("Index", "Home", "System");
            }
            TempData["error"] = "Mật khẩu hoặc tài khoản không chính xác!";
            ViewBag.UserName = model.Username;
            ViewBag.Password = model.Password;
            return View();
        }
        [HttpGet]
        //[AllowAnonymous]
        public IActionResult Register()
        {
            return View();
        }
        [HttpPost]
        [AllowAnonymous]
        public async Task<IActionResult> Register(RegisterViewModel model)
        {
            if (ModelState.IsValid)
            {
                User user = new User() { UserName = model.Username,FullName = model.Username };
                user.Email = "Admin@gmail.com";
                user.EmailConfirmed = true;
                var result = await _userManager.CreateAsync(user, model.Password);

                if (result.Succeeded)
                {
                    await _signInManager.SignInAsync(user, isPersistent: false);
                    return RedirectToAction("Index", "Homes", "Client");
                }
            }
            return View(model);
        }

        public async Task<IActionResult> Logout()
        {
            await _signInManager.SignOutAsync();
            return RedirectToAction("Login", "Accounts");
        }
    }
}