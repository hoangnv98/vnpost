﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Filters;
using Microsoft.AspNetCore.Routing;
using TinTuc.Data.Entities.System;
using TinTuc.Service.System;

namespace TinTuc.WebApplication.Areas.System.Controllers
{
	public class BaseController : Controller
	{
		private readonly UserManager<User> _userManager;
		private readonly SignInManager<User> _signInManager;
		private readonly IUserService _userService;
		public BaseController(
			UserManager<User> userManager,
			SignInManager<User> signInManager,
			IUserService userService)
		{
			_userManager = userManager;
			_signInManager = signInManager;
			_userService = userService;
		}
		public static User userLogin;
		public override void OnActionExecuting(ActionExecutingContext context)
		{
			if (!_signInManager.IsSignedIn(User))
			{
				context.Result = new RedirectToRouteResult(new RouteValueDictionary(
				new { action = "Login", controller = "Login", area = "System" }));
			}
			else
			{
				userLogin = _userService.GetUserByUserName(HttpContext.User.Identity.Name);
			}
		}
	}
}