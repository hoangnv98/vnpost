﻿    using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using TinTuc.Service.System;

namespace TinTuc.WebApplication.Areas.System.Controllers
{
    [Area("System")]
    public class HomeController : Controller
    {

        private readonly ICategoryService _categoryService;
        private readonly IArticleService _articleService;
        private readonly IUserService _userService;
        public HomeController(IArticleService articleService, ICategoryService categoryService, IUserService userService)
        {
            _articleService = articleService;
            _categoryService = categoryService;
            _userService = userService;
        }
        public IActionResult Index()
        {
            ViewBag.countArticle = _articleService.Count();
            ViewBag.countCategory = _categoryService.Count();
            ViewBag.countUser = _userService.Count();
            return View();
        }
    }
}