﻿var TABLE = {};
var table;
TABLE.init = () => {

};
TABLE.get = () => {
    $.ajax({
        "url": "/System/User/GetAll",
        "type": "GET",
        "success": function (data) {
            console.log(data);
            table = $('#table-users').dataTable({
                "aaData": data.models,
                ////"serverSide": true,
                "columns": [
                    {
                        'data': null,
                        "render": function (data, type, full, meta) {
                            return meta.row + 1;
                        }
                    },

                    { "data": "fullName" },
                    { "data": "userName" },
                    { "data": "email" },
                    {
                        'data': "gender",
                        "render": function (data, type, full, meta) {
                            return data == true ? `<span class="pcoded-badge badge badge-success">Nam</span>` : `<span class="pcoded-badge badge badge-danger">Nữ</span>`;
                        }
                    },
                    {
                        'data': "phoneNumber",
                        "render": function (data, type, full, meta) {
                            return data != null ? `<span class="pcoded-badge badge badge-success">` + data + `</span>` : `<span class="pcoded-badge badge badge-danger">No</span>`;
                        }
                    },
                    {
                        'data': "lockoutEnabled",
                        "render": function (data, type, full, meta) {
                            return data == false ? `<span class="pcoded-badge badge badge-success">No Locked</span>` : `<span class="pcoded-badge badge badge-danger">Locked</span>`;
                        }
                    },
                    {
                        'data': "id",
                        "render": function (data, type, full, meta) {
                            return `<button type="button" class="btn btn-icon btn-warning " onclick="editUser('` + data + `')" data-toggle="modal"
                                                data-target="#UserModal" ><i class="feather icon-alert-triangle"></i></button>`;
                        }
                    },
                ],
                "ordering": true
            });
        }
    });
}
$(document).ready(function () {
    TABLE.init();
    TABLE.get();
});
// Show Modal Create User
function ShowModalCreateUser() {
    var Id = '00000000-0000-0000-0000-000000000000';
    $('#exampleModalLabel').text('Create User');
    var id = $('input[name="id"]').val(Id);
    $('input[name="fullname"]').val('');
    $('input[name="userName"]').val('');
    $('input[name="email"]').val('');
    $('input[name="phone"]').val('');
    $('#gender').val("male");
    $('#LockoutEnabled').val("false");
    $('#UserModal').modal('toggle');
}
$('#btn-create-user-modal').click(function () {
    
    var id = $('input[name="id"]').val();
    var FullName = $('input[name="fullname"]').val();
    var UserName = $('input[name="userName"]').val();
    var Email = $('input[name="email"]').val();
    var PhoneNumber = $('input[name="phone"]').val();
    var Gender = $('#gender').val() == 'male' ? true : false;
    var PasswordHash = $('input[name="password"]').val();
    var LockoutEnabled = $('#LockoutEnabled').val();
    var dataCreateOrUpdate = {
        Model: {
            Id: id,
            FullName: FullName,
            UserName: UserName,
            Email: Email,
            PhoneNumber: PhoneNumber,
            Gender: Gender,
            PasswordHash: PasswordHash,
            LockoutEnabled: LockoutEnabled,
        }
    }
    console.log('data createOrUpdate', dataCreateOrUpdate.Model);
    $.ajax({
        "url": "/System/User/CreateOrUpdate",
        "type": "POST",
        "data": dataCreateOrUpdate,
        "success": function (data) {
            console.log('data', data);
            $('#UserModal').modal('hide');
            if (data.responseMessage == 'Thành công') {
                swal("Successfully!")
                    .then((value) => {
                        ReloadPage(500);
                    });
            }

            //console.log('data', data);
            //$('input[name="Name"]').val(data.model.name);
            //$('input[name="id"]').val(data.model.id);
            ////Add option to Parent Select
            //LoadParentCategory();
            //$("#Parent").val(data.model.parentId == null ? '' : data.model.parentId);
            //$("#isShow").val(data.model.status == 1 ? 'Active' : 'InActive');
            //$('textarea[name="Description"]').val(data.model.description);

            //$("button#btn-create-category-modal").addClass("edit-category");
        }
    });
});
//Edit User
function editUser(userId) {
    console.log('user edit: ', userId);
    $('input[name="id"]').val(userId);
    $('#exampleModalLabel').text('Edit User');

    $.ajax({
        "url": "/System/User/GetUserById",
        "type": "GET",
        "data": { id: userId },
        "success": function (data) {
            console.log(data);
            $('input[name="fullname"]').val(data.model.fullName);
            $('input[name="userName"]').val(data.model.userName);
            $('input[name="email"]').val(data.model.email);
            $('input[name="phone"]').val(data.model.phoneNumber);
            $('#gender').val(data.model.gender == true ? "male" : "female");
            $('#LockoutEnabled').val(data.model.lockoutEnabled == false ? "false" : "true");
            console.log('data', data);
            //$("button#btn-create-category-modal").addClass("edit-category");
        }
    });
}
function ReloadPage(time) {
    setTimeout(function () { location.reload(); }, time);
}
