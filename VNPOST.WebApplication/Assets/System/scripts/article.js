﻿var listArticle;
var listCate;
var countAddCategoryToSelectOption = true;
const baseUrl = window.location.protocol + "//" + window.location.host;
$(document).ready(function () {
    //Get All Category Showing
    $.ajax({
        "url": "/System/Category/GetAllCateShow",
        "type": "GET",
        "success": function (data) {
            listCate = data.models;
            console.log('list Cate: ', listCate);
            LoadCategory();
            $('#Category').selectpicker();
        }
    });
    //Get All Article
    $.ajax({
        "url": "/System/Article/GetAll",
        "type": "GET",
        "datatype": 'json',
        "success": function (data) {
            listArticle = data.models;
            console.log(listArticle);
            $('#table-articles').DataTable({

                data: data.models,
                columns: [
                    {
                        'data': null,
                        "render": function (data, type, full, meta) {
                            return meta.row + 1;
                        }
                    },
                    {
                        'data': 'title',
                        className: 'text-left text-nowrap',
                    },

                    {
                        'data': "thumbnail",
                        "render": function (data, type, full, meta) {
                            return `<img src="` + data + `" alt="" style="height: 150px;width: 250px;">`;
                        }
                    },
                    {
                        'data': "status",
                        "render": function (data, type, full, meta) {
                            return data == 1 ? `<span class="pcoded-badge badge badge-success">Show</span>` : `<span class="pcoded-badge badge badge-danger">Not Show</span>`;
                        }
                    },
                    {
                        'data': "user",
                        "render": function (data, type, full, meta) {
                            return data.fullName;
                        }
                    },
                    {
                        'data': "id",
                        "render": function (data, type, full, meta) {
                            return `<a type="button" class="btn btn-icon btn-warning btn-show-edit-article" href="/System/Article/Edit/`+data+`" onclick="editArticle('` + data + `')"><i class="feather icon-alert-triangle"></i></a>
                                <button type="button" class="btn btn-icon btn-danger" onclick="deleteArticle('` + data + `')"><i class="feather icon-slash"></i></button>`;
                        }
                    },
                ],
                
            });
        }
    });
});

//Load Category
function LoadCategory() {
    $('#Category').attr('enabled', 'true');
    if (countAddCategoryToSelectOption) {
        $.each(listCate, function () {
            var o = new Option(this.name, this.id);
            $("#Category").append(o);
        });
        countAddCategoryToSelectOption = false;
    }
};
//Preview Thumbnail
$("#Thumbnail").change(function () {
    readURL(this);
});
function readURL(input) {
    if (input.files && input.files[0]) {
        var reader = new FileReader();

        reader.onload = function (e) {
            $('#blah').attr('src', e.target.result);
        }

        reader.readAsDataURL(input.files[0]);
    }
}
// click create article
$('.btn-create-article').click(function () {
    var Id = '00000000-0000-0000-0000-000000000000';
    var Title = $('input[name="Title"]').val();
    console.log(CKEDITOR.instances.ArticleContent.getData());
    var Content = CKEDITOR.instances.ArticleContent.getData()
    var Thumbnail = $('#ImageValue').val();
    var lstCate = $('#Category').val();
    var Status = $('#isShow').val();
    var data = {
        Model: {
            Id: Id,
            Title: Title,
            Content: Content,
            Thumbnail: Thumbnail,
            ListCategory: lstCate,
            Status: Status
        }
    }
    console.log(data);
    $.ajax({
        "url": "/System/Article/CreateOrUpdate",
        "type": "POST",
        "data": data,
        "success": function (data) {
            data
            console.log('list Cate: ', data);
            window.location.href = baseUrl + '/System/Article/Index';
        }
    });
})
//click edit article 
$('.btn-edit-create').click(function () {
    var Id = $('input[name="id"]').val();
    var Title = $('input[name="Title"]').val();
    var Content = CKEDITOR.instances.ArticleContent.getData();
    console.log('content', Content);
    var Thumbnail = $('#ImageValue').val();
    var lstCate = $('#Category').val();
    var Status = $('#isShow').val();
    var data = {
        Model: {
            Id: Id,
            Title: Title,
            Content: Content,
            Thumbnail: Thumbnail,
            ListCategory: lstCate,
            Status: Status
        }
    }
    $.ajax({
        "url": "/System/Article/CreateOrUpdate",
        "type": "POST",
        "data": data,
        "success": function (data) {
            swal(" Edit Successfully!")
                .then((value) => {
                    window.location.href = baseUrl + '/System/Article/Index';
                });
        }
    });
})
// Convert Base64
$("#Thumbnail").on("change", function () {
    var filesSelected = document.getElementById("Thumbnail").files;
    if (filesSelected.length > 0) {
        var fileToLoad = filesSelected[0];
        var fileReader = new FileReader();
        fileReader.onload = function (fileLoadedEvent) {
            var srcData = fileLoadedEvent.target.result; // <--- data: base64
            $("#ImageValue").attr("value", srcData);
        }
        fileReader.readAsDataURL(fileToLoad);
    }
});
//Delete Article
function deleteArticle(data) {
    swal({
        title: "Are you sure?",
        text: "You must delete Article",
        icon: "warning",
        buttons: true,
        dangerMode: true,
    })
        .then((willDelete) => {
            if (willDelete) {
                $.ajax({
                    "url": "/System/Article/Delete",
                    "type": "GET",
                    "data": {
                        id: data
                    },
                    "success": function (data) {
                        if (data.responseMessage == 'Thành công') {

                            swal(" Successfully!")
                                .then((value) => {
                                    ReloadPage(500);
                                });
                        }

                        //console.log('data', data);
                        //$('input[name="Name"]').val(data.model.name);
                        //$('input[name="id"]').val(data.model.id);
                        ////Add option to Parent Select
                        //LoadParentCategory();
                        //$("#Parent").val(data.model.parentId == null ? '' : data.model.parentId);
                        //$("#isShow").val(data.model.status == 1 ? 'Active' : 'InActive');
                        //$('textarea[name="Description"]').val(data.model.description);

                        //$("button#btn-create-category-modal").addClass("edit-category");
                    }
                });
                swal("Category has been deleted!", {
                    icon: "success",
                });
            } else {
                //swal("Category file is safe!");
            }
        });
}
//click show Edit Article
function editArticle(id) {
    console.log('article edit id: ', id);
    //$.ajax({
    //    "url": "/System/Article/Edit",
    //    "type": "GET",
    //    "data": { id: id },
    //    "success": function (data) {
    //        //console.log('data', data);
    //        //$('input[name="Name"]').val(data.model.name);
    //        //$('input[name="id"]').val(data.model.id);
    //        ////Add option to Parent Select
    //        //LoadParentCategory();
    //        //$("#Parent").val(data.model.parentId == null ? '' : data.model.parentId);
    //        //$("#isShow").val(data.model.status == 1 ? 'Active' : 'InActive');
    //        //$('textarea[name="Description"]').val(data.model.description);

    //        //$("button#btn-create-category-modal").addClass("edit-category");
    //    }
    //});
}
function ReloadPage(time) {
    setTimeout(function () { location.reload(); }, time);
}