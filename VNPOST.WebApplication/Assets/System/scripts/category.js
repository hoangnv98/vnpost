﻿var TABLE = {};
var listCate;
var countAddCategoryToSelectOption = true;
var tableCategory;
TABLE.init = () => {

};
TABLE.get = () => {
    $.ajax({
        "url": "/System/Category/GetAll",
        "type": "GET",
        "success": function (data) {
            console.log(data)
            listCate = data.models;
            tableCategory = $('#table-categories').dataTable({
                "aaData": listCate,
                //"serverSide": true,
                "columns": [
                    {
                        'data': null,
                        "render": function (data, type, full, meta) {
                            return meta.row + 1;
                        }
                    },

                    { "data": "name" },
                    { "data": "description" },
                    {
                        'data': "parentName",
                        "render": function (data, type, full, meta) {
                            return data == null ? `<span class="pcoded-badge badge badge-danger">None</span>` : `<span class="pcoded-badge badge badge-success">` + data + `</span>`;
                        }
                    },
                    {
                        'data': "status",
                        "render": function (data, type, full, meta) {
                            return data == 1 ? `<span class="pcoded-badge badge badge-success">Show</span>` : `<span class="pcoded-badge badge badge-danger">Not Show</span>`;
                        }
                    },
                    {
                        'data': "user",
                        "render": function (data, type, full, meta) {
                            return data.fullName;
                        }
                    },
                    {
                        'data': "id",
                        "render": function (data, type, full, meta) {
                            return `<button type="button" class="btn btn-icon btn-warning " onclick="editCategory('` + data + `')" data-toggle="modal"
                                                data-target="#CategoryModal" ><i class="feather icon-alert-triangle"></i></button>
                                <button type="button" class="btn btn-icon btn-danger" onclick="deleteCategory('` + data + `')"><i class="feather icon-slash"></i></button>`;
                        }
                    },
                ],
                "ordering": true
            });
        }
    });
}


$(document).ready(function () {
    TABLE.init();
    TABLE.get();
});
//click createOrUpdate Category
$("#btn-create-category-modal").click(function () {
    var id = $('input[name="id"]').val();
    var name = $('input[name="Name"]').val();
    var description = $('textarea[name="Description"]').val();
    var parentId = $('#Parent').val();
    var status = $('#isShow').val();
    var dataCreateOrUpdate = {
        Model: {
            Id: id,
            Name: name,
            Description: description,
            ParentId: parentId,
            Status: status,
        }
    }
    console.log('data createOrUpdate', dataCreateOrUpdate.Model);
    $.ajax({
        "url": "/System/Category/CreateOrUpdate",
        "type": "POST",
        "data": dataCreateOrUpdate,
        "success": function (data) {
            console.log('data', data);
            $('#CategoryModal').modal('hide');
            if (data.responseMessage == 'Thành công') {
                swal(" Successfully!")
                    .then((value) => {
                        ReloadPage(500);
                    });
            }

            //console.log('data', data);
            //$('input[name="Name"]').val(data.model.name);
            //$('input[name="id"]').val(data.model.id);
            ////Add option to Parent Select
            //LoadParentCategory();
            //$("#Parent").val(data.model.parentId == null ? '' : data.model.parentId);
            //$("#isShow").val(data.model.status == 1 ? 'Active' : 'InActive');
            //$('textarea[name="Description"]').val(data.model.description);

            //$("button#btn-create-category-modal").addClass("edit-category");
        }
    });
});
//click Delete Category
function deleteCategory(data) {
    swal({
        title: "Are you sure?",
        text: "You must delete child Category",
        icon: "warning",
        buttons: true,
        dangerMode: true,
    })
        .then((willDelete) => {
            if (willDelete) {
                $.ajax({
                    "url": "/System/Category/Delete",
                    "type": "GET",
                    "data": {
                        id: data
                    },
                    "success": function (data) {
                        if (data.responseMessage == 'Thành công') {
                            
                            swal(" Successfully!")
                                .then((value) => {
                                    ReloadPage(500);
                                });
                        }

                        //console.log('data', data);
                        //$('input[name="Name"]').val(data.model.name);
                        //$('input[name="id"]').val(data.model.id);
                        ////Add option to Parent Select
                        //LoadParentCategory();
                        //$("#Parent").val(data.model.parentId == null ? '' : data.model.parentId);
                        //$("#isShow").val(data.model.status == 1 ? 'Active' : 'InActive');
                        //$('textarea[name="Description"]').val(data.model.description);

                        //$("button#btn-create-category-modal").addClass("edit-category");
                    }
                });
                swal("Category has been deleted!", {
                    icon: "success",
                });
            } else {
                //swal("Category file is safe!");
            }
        });
}
// Show Modal Create Category
function ShowModalCreateCategory() {
    var Id = '00000000-0000-0000-0000-000000000000';
    $('#exampleModalLabel').text('Create Category');
    console.log('listcategory', listCate);
    var id = $('input[name="id"]').val(Id);
    $('input[name="Name"]').val('');
    $('textarea[name="Description"]').val('');
    $('#CategoryModal').modal('toggle');
    LoadParentCategory();
    //$('#Parent')
    //    .find('option')
    //    .remove()
    //    .end()
    //    .append('<option value="whatever">text</option>')
    //    .val('whatever')
    //    ;
}
// Show Modal Edit Category
function editCategory(cateId) {
    console.log('cateId edit: ', cateId);
    $.ajax({
        "url": "/System/Category/GetCateById",
        "type": "GET",
        "data": { id: cateId },
        "success": function (data) {
            console.log('data', data);
            $('input[name="Name"]').val(data.model.name);
            $('input[name="id"]').val(data.model.id);
            //Add option to Parent Select
            LoadParentCategory();
            $("#Parent").val(data.model.parentId == null ? '' : data.model.parentId);
            $("#isShow").val(data.model.status == 1 ? 'Active' : 'InActive');
            $('textarea[name="Description"]').val(data.model.description);

            //$("button#btn-create-category-modal").addClass("edit-category");
        }
    });
}
// Load Parent Category
function LoadParentCategory() {
    $('#Parent').attr('enabled', 'true');
    if (countAddCategoryToSelectOption) {
        $('#Parent').append(
            $("<option></option>").text('No Parent').val('')
        );
        $.each(listCate, function () {
            $('#Parent').append(
                $("<option></option>").text(this.name).val(this.id)
            );
        });
        countAddCategoryToSelectOption = false;
    }
};
// Reload Page
function ReloadPage(time) {
    setTimeout(function () { location.reload(); }, time);
}
