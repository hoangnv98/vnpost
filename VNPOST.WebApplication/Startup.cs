using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.HttpsPolicy;
using Microsoft.AspNetCore.Mvc.Razor;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.FileProviders;
using Microsoft.Extensions.Hosting;
using TinTuc.Service.Se;
using TinTuc.Data.EF;
using TinTuc.Data.Infrastructure;
using TinTuc.Service.System;
using TinTuc.Data.Entities.System;

namespace TinTuc.WebApplication
{
	public class Startup
	{
		public Startup(IConfiguration configuration)
		{
			Configuration = configuration;
		}

		public IConfiguration Configuration { get; }

		// This method gets called by the runtime. Use this method to add services to the container.
		public void ConfigureServices(IServiceCollection services)
		{
			services.AddIdentity<User, Role>(options =>
			{
				options.Password.RequiredLength = 3;
				options.Password.RequiredUniqueChars = 3;
				options.Password.RequireDigit = false;
				options.Password.RequireNonAlphanumeric = false;
			}).AddEntityFrameworkStores<TinTucDbContext>();
			services.AddControllersWithViews();
			//services.AddDbContext<BloggingContext>(options => options.UseSqlite("Data Source=blog.db"));
			services.AddDbContext<TinTucDbContext>(options => options.UseSqlServer(Configuration.GetConnectionString("TinTucDb")));
			services.AddScoped<IArticleService, ArticleService>();
			services.AddScoped<ICategoryService, CategoryService>();
			services.AddScoped<IUserService, UserService>();
			services.AddScoped<ICategoryPublicService, CategoryPublicService>();
			services.AddScoped<IArticlePublicService, ArticlePublicService>();
			services.AddMvc().AddNewtonsoftJson(options =>
			{
				options.SerializerSettings.ReferenceLoopHandling = Newtonsoft.Json.ReferenceLoopHandling.Ignore;
			});
			services.AddControllersWithViews().AddNewtonsoftJson(options =>
	options.SerializerSettings.ReferenceLoopHandling = Newtonsoft.Json.ReferenceLoopHandling.Ignore
);
			//services.AddTransient <IUnitOfWork<TinTucDbContext>,UnitOfWork<CoreSampe>	;
			/*
			services.AddScoped<irepositoryfactory, unitofwork<coresamplecontext="">>();
			services.AddScoped<iunitofwork, unitofwork<coresamplecontext="">>();
			services.AddScoped<iunitofwork<akoubacontext>, UnitOfWork<coresamplecontext>>();
			 
			 */
			//services.Configure<RazorViewEngineOptions>(options =>
			//{
			//	options.AreaViewLocationFormats.Clear();
			//	options.AreaViewLocationFormats.Add("/MyArea/{2}/Views/{1}/{0}.cshtml");
			//	options.AreaViewLocationFormats.Add("/MyArea/{2}/Views/Shared/{0}.cshtml");
			//	options.AreaViewLocationFormats.Add("/Views/Shared/{0}.cshtml");
			//});
		}

		// This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
		public void Configure(IApplicationBuilder app, IWebHostEnvironment env)
		{
			if (env.IsDevelopment())
			{
				app.UseDeveloperExceptionPage();
			}
			else
			{
				app.UseExceptionHandler("/Home/Error");
				// The default HSTS value is 30 days. You may want to change this for production scenarios, see https://aka.ms/aspnetcore-hsts.
				app.UseHsts();
			}
			app.UseHttpsRedirection();
			app.UseStaticFiles();

			app.UseRouting();
			app.UseAuthentication();	
			app.UseAuthorization();

			app.UseEndpoints(endpoints =>
			{
				endpoints.MapAreaControllerRoute(
					name: "MyAreaSe",
					areaName: "Se",
					pattern: "{controller=Home}/{action=Index}/{id?}");

				endpoints.MapAreaControllerRoute(
					name: "MyAreaSystem",
					areaName: "System",
					pattern: "System/{controller=Login}/{action=SignIn}/{id?}");

				//endpoints.MapControllerRoute(
				//	name: "MyArea",
				//	pattern: "{area:exists}/{controller=CategoryTypes}/{action=Index}/{id?}");

				//endpoints.MapControllerRoute(
				//	name: "default",
				//	pattern: "{area:exists}/{controller=Home}/{action=Index}/{id?}");
			});
			app.UseStaticFiles();
			app.UseStaticFiles(new StaticFileOptions()
			{
				FileProvider = new PhysicalFileProvider(
					Path.Combine(Directory.GetCurrentDirectory(), @"Assets")),
				RequestPath = new PathString("/Assets")
			});
		}
	}
}
