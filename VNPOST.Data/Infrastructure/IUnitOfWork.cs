﻿using System;
using System.Collections.Generic;
using System.Text;
using TinTuc.Data.Entities;
using TinTuc.Data.Entities.System;

namespace TinTuc.Data.Infrastructure
{
	public interface IUnitOfWork
	{
		// System
		IRepository<User> UserReponsitory { get; }
		IRepository<Article> ArticleRepository { get; }
		IRepository<ArticleCategory> ArticleCategoryRepository { get; }
		IRepository<Category> CategoryRepository { get; }

		// Register


		/// <summary>
		/// Commit all change
		/// </summary>
		void Commit();

	}
}
