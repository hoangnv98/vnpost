﻿using System;
using System.Collections.Generic;
using System.Text;
using TinTuc.Data.Entities.System;
using TinTuc.Data.Enums;

namespace TinTuc.Data.Entities
{
	public class Article : BaseEntity
	{
		public string Title { get; set; }

		public string TitleNormalize { get; set; }

		public string Description { get; set; }
		public string Thumbnail { get; set; }

		public string Tags { get; set; }
		public string Content { get; set; }
		public Status Status { get; set; }

		public Guid UserId { get; set; }
		public virtual User User { get; set; }
		public virtual ICollection<ArticleCategory> ArticleCategories { get; set; }

	}
}
