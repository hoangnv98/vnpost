﻿using System;
using System.Collections.Generic;
using System.Text;
using TinTuc.Data.Entities.System;
using TinTuc.Data.Enums;

namespace TinTuc.Data.Entities
{
	public class Category : BaseEntity
	{
		public string Name { get; set; }

		public Guid? ParentId { get; set; }

		public string Description { get; set; }
		public string ImageUrl { get; set; }
		public string ParentName { get; set; }

		public Status Status { get; set; }
		public Guid UserId { get; set; }

		public virtual User User { get; set; }
		public virtual ICollection<ArticleCategory> ArticleCategories { get; set; }
	}
}
