﻿using Microsoft.AspNetCore.Identity;
using System;
using System.Collections.Generic;
using System.Text;

namespace TinTuc.Data.Entities.System
{
	public class User : IdentityUser<Guid>
	{

		//[Required]
		//[StringLength(100)]
		public string FullName { get; set; }

		public bool Gender { get; set; }

		//[Required]
		//[StringLength(128)]

		public DateTime? LastLogin { get; set; }

		public DateTime BirthDate { get; set; }
		public DateTime CreatedAt { get; set; }

		//[StringLength(50), Phone]
		public string Tel { get; set; }

		public string Avatar { get; set; }

		public DateTime? CheckChange { get; set; }
		//public List<Article> Articles { get; set; }
		//public List<Category> Categories { get; set; }

	}
}
