﻿using System;
using System.Collections.Generic;
using System.Text;

namespace TinTuc.Data.Entities
{
	public class BaseEntity
	{
		protected BaseEntity()
		{
			Id = Guid.NewGuid();
		}
		public Guid Id { get; set; }
		public string CreatedBy { get; set; }

		public DateTime? CreatedAt { get; set; }
		public string ModifiedBy { get; set; }
		public DateTime? ModifiedAt { get; set; }
	}
}
