﻿using System;
using System.Collections.Generic;
using System.Text;
using TinTuc.Data.Enums;

namespace TinTuc.Data.Entities
{
	public class ArticleCategory : BaseEntity
	{
		public Guid? ArticleId { get; set; }
		public Guid? CategoryId { get; set; }
		public Status? Status { get; set; }
		public virtual Article Article { get; set; }
		public virtual Category Category { get; set; }
	}
}
