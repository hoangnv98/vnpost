﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using System;
using System.Collections.Generic;
using System.Text;
using TinTuc.Data.Entities.System;

namespace TinTuc.Data.Configurations.System
{
	public class UserConfiguration : IEntityTypeConfiguration<User>
	{
		public void Configure(EntityTypeBuilder<User> builder)
		{
			builder.ToTable("AppUsers");
			builder.HasKey(x => x.Id);
			builder.Property(x => x.UserName).HasMaxLength(200).IsRequired();
			builder.Property(x => x.PasswordHash).IsRequired();
			builder.Property(x => x.Tel).HasMaxLength(50);
			builder.Property(x => x.Email).IsRequired();
		}
	}
}
