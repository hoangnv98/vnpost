﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using System;
using System.Collections.Generic;
using System.Text;
using TinTuc.Data.Entities.System;

namespace TinTuc.Data.Configurations.System
{
	public class RoleConfiguration : IEntityTypeConfiguration<Role>
	{
		public void Configure(EntityTypeBuilder<Role> builder)
		{
			builder.ToTable("AppRoles");
			builder.Property(x => x.Description).HasMaxLength(200).IsRequired();
			
		}
	}
}
