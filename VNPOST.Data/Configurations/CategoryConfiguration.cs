﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using System;
using System.Collections.Generic;
using System.Text;
using TinTuc.Data.Entities;
using TinTuc.Data.Enums;

namespace TinTuc.Data.Configurations
{
	public class CategoryConfiguration : IEntityTypeConfiguration<Category>
	{
		public void Configure(EntityTypeBuilder<Category> builder)
		{
			builder.ToTable("Categories");
			builder.HasKey(x => x.Id);
			builder.Property(x => x.Name).IsRequired(true);
			builder.Property(x => x.Status).HasDefaultValue(Status.Active);
			builder.Property(x => x.ParentId).IsRequired(false);
			builder.Property(x => x.ModifiedBy).IsRequired(false);
			builder.Property(x => x.ModifiedAt).IsRequired(false);
			builder.Property(x => x.ParentName).IsRequired(false);
			builder.Property(x => x.ImageUrl).IsRequired(false);
			//builder.HasOne(x => x.User).WithMany(x => x.Categories).HasForeignKey(x => x.UserId);

		}
	}
}
