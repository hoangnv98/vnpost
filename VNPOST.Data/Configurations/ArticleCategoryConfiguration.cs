﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using System;
using System.Collections.Generic;
using System.Text;
using TinTuc.Data.Entities;

namespace TinTuc.Data.Configurations
{
	public class ArticleCategoryConfiguration : IEntityTypeConfiguration<ArticleCategory>
	{
		public void Configure(EntityTypeBuilder<ArticleCategory> builder)
		{
			builder.HasKey(x => x.Id);
			
			//modelBuilder.Entity() .HasRequired(c => c.Stage) .WithMany() .WillCascadeOnDelete(false);  
			builder.HasOne(x => x.Article).WithMany(ac => ac.ArticleCategories)
				.HasForeignKey(ac => ac.ArticleId).OnDelete(DeleteBehavior.Cascade);
			builder.HasOne(x => x.Category).WithMany(ac => ac.ArticleCategories)
				.HasForeignKey(ac => ac.CategoryId).OnDelete(DeleteBehavior.Cascade);
		}
	}
}
