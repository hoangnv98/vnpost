﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using System;
using System.Collections.Generic;
using System.Text;
using TinTuc.Data.Entities;

namespace TinTuc.Data.Configurations
{
	public class ArticleConfiguration : IEntityTypeConfiguration<Article>
	{
		public void Configure(EntityTypeBuilder<Article> builder)
		{
			builder.ToTable("Articles");
			builder.HasKey(x => x.Id);
			builder.Property(x => x.Title).IsRequired();
			builder.Property(x => x.ModifiedBy).IsRequired(false);
			builder.Property(x => x.Thumbnail).IsRequired(false);
			builder.Property(x => x.Tags).IsRequired(false);
			builder.Property(x => x.TitleNormalize).IsRequired(false);
			//builder.HasOne(x => x.User).WithMany(x => x.Articles).HasForeignKey(x => x.UserId);

		}
	}
}
