﻿using Microsoft.AspNetCore.Identity;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Data.SqlTypes;
using System.Text;
using TinTuc.Data.Entities;
using TinTuc.Data.Entities.System;
using TinTuc.Data.Enums;

namespace TinTuc.Data.Extensions
{
	public static class ModelBuilderExtensions
	{
		public static void Seed(this ModelBuilder modelBuilder)
		{

			var roleIDAdmin = new Guid("436B9ACE-D164-4B7C-9A4B-5C6356AA29CD");
			var roleIDUser = new Guid("88FF2142-9037-4DA8-A700-CDE30EFD2592");
			var adminID = new Guid("B522DBA4-E9DC-4A0E-BAA8-DACF6477DAB9");
			var userID = new Guid("8B26D604-F1AE-4541-B182-0F4D838EC3EE");
			/* Role Document*/
			modelBuilder.Entity<Role>().HasData(new Role
			{
				Id = roleIDAdmin,
				Name = "admin",
				NormalizedName = "admin",
				Description = "Administrator Role"
			});
			modelBuilder.Entity<Role>().HasData(new Role
			{
				Id = roleIDUser,
				Name = "User",
				NormalizedName = "User",
				Description = "User Role"
			});

			var hasher = new PasswordHasher<User>();

			modelBuilder.Entity<User>().HasData(new User
			{
				Id = adminID,
				UserName = "admin",
				NormalizedUserName = "admin",
				Email = "admin@news.com",
				NormalizedEmail = "admin@news.com",
				EmailConfirmed = true,
				PasswordHash = hasher.HashPassword(null, "1234@"),
				SecurityStamp = string.Empty,
				FullName = "Administrator",
				BirthDate = new DateTime(1998, 12, 28)
			});
			modelBuilder.Entity<User>().HasData(new User
			{
				Id = userID,
				UserName = "Nguyen Viet Hoang",
				NormalizedUserName = "nguyenviethoang",
				Email = "nvh@news.com",
				NormalizedEmail = "nvh@news.com",
				EmailConfirmed = true,
				PasswordHash = hasher.HashPassword(null, "1234@"),
				SecurityStamp = string.Empty,
				FullName = "Nguyen Viet Hoang",
				BirthDate = new DateTime(1998, 12, 28)
			});

			modelBuilder.Entity<IdentityUserRole<Guid>>().HasData(new IdentityUserRole<Guid>
			{
				RoleId = roleIDAdmin,
				UserId = adminID
			});
			modelBuilder.Entity<IdentityUserRole<Guid>>().HasData(new IdentityUserRole<Guid>
			{
				RoleId = roleIDUser,
				UserId = userID
			});
			//var idArticleEdu = Guid.NewGuid();
			//modelBuilder.Entity<Article>().HasData(
			//	new Article
			//	{
			//		Id = idArticleEdu,
			//		Title = "Trường Đại học Khoa học Tự nhiên - ĐHQGHN có hiệu trưởng mới",
			//		Description = "Giám đốc ĐH Quốc gia Hà Nội vừa ký Quyết định bổ nhiệm PGS.TSKH. Vũ Hoàng Linh giữ chức vụ Hiệu trưởng Trường ĐH Khoa học Tự nhiên.",
			//		Tags = "hieutruong",
			//		Content = "Trước khi được bổ nhiệm, PGS. TS. Vũ Hoàng Linh là Phó hiệu trưởng trường ĐH Khoa học Tự nhiên, ĐH Quốc gia Hà Nội.",
			//		UserId = adminID,
			//	},
			//	new Article
			//	{
			//		Id = Guid.NewGuid(),
			//		Title = "Trường Đại học Khoa học Tự nhiên - ĐHQGHN có hiệu trưởng mới",
			//		Description = "Giám đốc ĐH Quốc gia Hà Nội vừa ký Quyết định bổ nhiệm PGS.TSKH. Vũ Hoàng Linh giữ chức vụ Hiệu trưởng Trường ĐH Khoa học Tự nhiên.",
			//		Tags = "hieutruong",
			//		Content = "Trước khi được bổ nhiệm, PGS. TS. Vũ Hoàng Linh là Phó hiệu trưởng trường ĐH Khoa học Tự nhiên, ĐH Quốc gia Hà Nội.",
			//		UserId = adminID,
			//	}
			//	);
			//var idCateEdu = Guid.NewGuid();
			//modelBuilder.Entity<Category>().HasData(
			//	new Category
			//	{
			//		Id = idCateEdu,
			//		Name = "Giáo dục",
			//		Description = "Giáo dục con người",
			//		CreatedBy = adminID.ToString(),
			//		Status = Status.Active,
			//		UserId = adminID,
			//	},
			//	new Category
			//	{
			//		Id = Guid.NewGuid(),
			//		Name = "Covid-19",
			//		Description = "Những tin tức về Covid-19",
			//		CreatedBy = adminID.ToString(),
			//		Status = Status.Active,
			//		UserId = adminID,
			//	},
			//	new Category
			//	{
			//		Id = Guid.NewGuid(),
			//		Name = "Giáo dục trẻ nhỏ",
			//		Description = "Giáo giục trẻ nhỏ",
			//		CreatedBy = adminID.ToString(),
			//		ParentId = idCateEdu,
			//		Status = Status.Active,
			//		UserId = adminID,
			//	}
			//); ;
			//modelBuilder.Entity<ArticleCategory>().HasData(new ArticleCategory
			//{
			//	ArticleId = idArticleEdu,
			//	CategoryId = idCateEdu,
			//});
			modelBuilder.Entity<ArticleCategory>().HasOne(c => c.Category)
				.WithMany(u => u.ArticleCategories).IsRequired().OnDelete(DeleteBehavior.NoAction);
			modelBuilder.Entity<ArticleCategory>().HasOne(c => c.Article)
				.WithMany(u => u.ArticleCategories).IsRequired().OnDelete(DeleteBehavior.NoAction);
		}
	}
}
