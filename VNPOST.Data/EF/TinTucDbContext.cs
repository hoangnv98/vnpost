﻿using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Identity.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Text;
using TinTuc.Data.Configurations;
using TinTuc.Data.Configurations.System;
using TinTuc.Data.Entities;
using TinTuc.Data.Entities.System;
using TinTuc.Data.Extensions;

namespace TinTuc.Data.EF
{
	public class TinTucDbContext : IdentityDbContext<User,Role,Guid>
	{
		public TinTucDbContext(DbContextOptions options) : base(options)
		{
			//options.UseSqlServer("");
		}
		protected override void OnModelCreating(ModelBuilder modelBuilder)
		{
			// Config Fluent API
			modelBuilder.ApplyConfiguration(new ArticleConfiguration());
			modelBuilder.ApplyConfiguration(new CategoryConfiguration());
			modelBuilder.ApplyConfiguration(new ArticleCategoryConfiguration());
			modelBuilder.ApplyConfiguration(new UserConfiguration());
			modelBuilder.ApplyConfiguration(new RoleConfiguration());

			modelBuilder.Entity<IdentityUserClaim<Guid>>().ToTable("AppUserClaims");
			modelBuilder.Entity<IdentityUserRole<Guid>>().ToTable("AppUserRoles").HasKey(x => new { x.UserId,x.RoleId });
			modelBuilder.Entity<IdentityUserLogin<Guid>>().ToTable("AppUserLogins").HasKey(x => x.UserId);
			modelBuilder.Entity<IdentityRoleClaim<Guid>>().ToTable("AppRoleClaims");
			modelBuilder.Entity<IdentityUserToken<Guid>>().ToTable("AppUserTokens").HasKey(x => x.UserId);

			modelBuilder.Entity<ArticleCategory>().HasKey(ac => new { ac.ArticleId, ac.CategoryId });
			//modelBuilder.Entity<ArticleCategory>().HasOne(x => x.Category).WithMany( a => )

			//builder.HasOne(x => x.User).WithMany(x => x.Articles).HasForeignKey(x => x.UserId);
			//
			//modelBuilder.Entity<ArticleCategory>().HasRequired(c => c.Stage).WithMany().WillCascadeOnDelete(false);  
			//Data Seeding
			modelBuilder.Seed();
			//base.OnModelCreating(modelBuilder);
		}
		public DbSet<Article> Articles { get; set; }
		public DbSet<Category> Categories { get; set; }
		public DbSet<ArticleCategory> ArticleCategories { get; set; }

	}
}
