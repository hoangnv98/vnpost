﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Design;
using Microsoft.Extensions.Configuration;
using System;
using System.Collections.Generic;
using System.IO;
using System.Text;

namespace TinTuc.Data.EF
{
	public class TinTucDbContextFactory : IDesignTimeDbContextFactory<TinTucDbContext>
	{
		public TinTucDbContext CreateDbContext(string[] args)
		{

			IConfigurationRoot configuration = new ConfigurationBuilder()
				.SetBasePath(Directory.GetCurrentDirectory())
				.AddJsonFile("appsettings.json")
				.Build();

			var connectionString = configuration.GetConnectionString("TinTucDb");
			var optionsBuilder = new DbContextOptionsBuilder<TinTucDbContext>();
			optionsBuilder.UseSqlServer(connectionString);

			return new TinTucDbContext(optionsBuilder.Options);
		}
	}
}
