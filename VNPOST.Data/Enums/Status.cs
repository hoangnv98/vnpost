﻿using System;
using System.Collections.Generic;
using System.Text;

namespace TinTuc.Data.Enums
{
	public enum Status
	{
		InActive = 0,
		Active = 1,
	}
}
