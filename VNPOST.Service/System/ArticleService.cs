﻿using Common;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web;
using TinTuc.Data.EF;
using TinTuc.Data.Entities;
using TinTuc.Data.Enums;
using TinTuc.Data.Infrastructure;
using TinTuc.Service.Models;
using TinTuc.Service.Models.Request;
using TinTuc.Service.Models.Response;
using TinTuc.Service.ViewModels;

namespace TinTuc.Service.System
{
	public class ArticleService : IArticleService
	{
		private readonly TinTucDbContext _dbContext;
		private readonly Guid UserId = new Guid("b522dba4-e9dc-4a0e-baa8-dacf6477dab9");

		public ArticleService(TinTucDbContext dbContext)
		{
			_dbContext = dbContext;
		}

		public int Count()
		{
			try
			{
				int count = _dbContext.Articles.Count();
				return count;
			}
			catch (Exception)
			{

				throw;
			}
		}

		public UpdateResponse CreateOrUpdate(UpdateRequest<ArticleViewModel> request)
		{
			try
			{
				var article = _dbContext.Articles.Find(request.Model.Id);

				if (article != null)
				{//ad593d14-ce78-43b7-9fdc-19d847e1501c
					article.Title = request.Model.Title?.Trim();
					article.Thumbnail = request.Model.Thumbnail != null ? request.Model.Thumbnail?.Trim() : article.Thumbnail;
					article.Content = request.Model.Content;
					article.ModifiedBy = UserId.ToString();
					article.ModifiedAt = DateTime.Now;
					article.Status = request.Model.Status;
						if (request.Model.ListCategory != null)
					{
						var listAc = _dbContext.ArticleCategories.Where(x => x.ArticleId == request.Model.Id).ToList();
						foreach (var item in listAc)
						{
							_dbContext.ArticleCategories.Attach(item);
							_dbContext.ArticleCategories.Remove(item);
						}
						foreach (var item in request.Model.ListCategory)
						{
							var ac = new ArticleCategory();
							ac.Id = Guid.NewGuid();
							ac.ArticleId = article.Id;
							ac.CategoryId = item;
							ac.CreatedAt = DateTime.Now;
							_dbContext.ArticleCategories.Add(ac);
						}
					}
					_dbContext.Articles.Update(article);
					_dbContext.SaveChanges();
					//_unitOfWork.ArticleRepository.Update(article);
					//_unitOfWork.Commit();
				}
				else
				{
					var _article = new Article();
					_article.Id = Guid.NewGuid();
					_article.Title = request.Model.Title?.Trim();
					_article.Content = request.Model.Content?.Trim();
					_article.Status = request.Model.Status;
					_article.Thumbnail = request.Model.Thumbnail?.Trim();
					_article.CreatedAt = DateTime.Now;
					_article.UserId = UserId;
					foreach (var item in request.Model.ListCategory)
					{
						var ac = new ArticleCategory();
						ac.Id = Guid.NewGuid();
						ac.ArticleId = _article.Id;
						ac.CategoryId = item;
						ac.CreatedAt = DateTime.Now;
						_dbContext.ArticleCategories.Add(ac);
					}
					_dbContext.Articles.Add(_article);
					_dbContext.SaveChanges();

				}
			}
			catch (Exception ex)
			{

				return new UpdateResponse
				{
					Status = ResponseStatus.Exception,
					ResponseMessage = ex.Message
				};
			}
			return new UpdateResponse
			{
				Id = request.Model.Id,
				ResponseMessage = "Thành công",
				Status = ResponseStatus.Success
			};
		}

		public DeleteResponse Delete(Guid id)
		{
			try
			{
				var article = _dbContext.Articles.Find(id);
				var ListAC = _dbContext.ArticleCategories.Where(x => x.ArticleId == article.Id).ToList();
				foreach (var item in ListAC)
				{
					_dbContext.ArticleCategories.Attach(item);
					_dbContext.ArticleCategories.Remove(item);
				}
				_dbContext.Articles.Attach(article);
				_dbContext.Articles.Remove(article);
				_dbContext.SaveChanges();
				return new DeleteResponse
				{
					Status = ResponseStatus.Success,
					ResponseMessage = "Thành công"
				};
			}
			catch (Exception ex)
			{
				return new DeleteResponse
				{
					Status = ResponseStatus.Exception,
					ResponseMessage = ex.Message
				};
			}
		}

		public GetResponse<Article> GetAll()
		{
			try
			{
				var list = _dbContext.Articles
					.Include(c => c.User).ToList();
				//foreach (var item in list)
				//{
				//	var x = _dbContext.ArticleCategories.Select(ac => ac.ArticleId == item.Id).ToList();
				//	//item.ArticleCategories = _dbContext.ArticleCategories.Where(ac => ac.ArticleId == ""));
				//}
				return new GetResponse<Article>
				{
					Models = list.OrderByDescending(e => e.CreatedAt),
					Status = ResponseStatus.Success
				};
			}
			catch (Exception e)
			{

				return new GetResponse<Article>
				{
					Status = ResponseStatus.Exception,
					ResponseMessage = e.Message
				};
			}
		}

		public DataTableJsResponse<Article> GetAll(DataTableJsRequest<Article> request, string q = null)
		{
			throw new NotImplementedException();
		}

		public PagedViewModel<ArticleViewModel> GetAllPaging(string key, int pageIndex, int pageSize)
		{
			throw new NotImplementedException();
		}

		public GetResponse<Article> GetAllShow()
		{
			try
			{
				var list = _dbContext.Articles.Where(x => x.Status == Status.Active).Include(b => b.ArticleCategories)
					.Include(c => c.User).ToList();
				//foreach (var item in list)
				//{
				//	var x = _dbContext.ArticleCategories.Select(ac => ac.ArticleId == item.Id).ToList();
				//	//item.ArticleCategories = _dbContext.ArticleCategories.Where(ac => ac.ArticleId == ""));
				//}
				return new GetResponse<Article>
				{
					Models = list.OrderByDescending(e => e.CreatedAt),
					Status = ResponseStatus.Success
				};
			}
			catch (Exception e)
			{

				return new GetResponse<Article>
				{
					Status = ResponseStatus.Exception,
					ResponseMessage = e.Message
				};
			}
		}

		public GetResponse<Article> GetArticleForCategory(Guid cateid)
		{
			try
			{
				var listArticle = _dbContext.ArticleCategories.Where(x => x.CategoryId == cateid).ToList();
				List<Article> lstArticle = new List<Article>();

				foreach (var item in listArticle)
				{
					var article = _dbContext.Articles.Where(x => x.Id == item.ArticleId).FirstOrDefault();
					lstArticle.Add(article);
				}
				return new GetResponse<Article>
				{
					Models = lstArticle,
					Status = ResponseStatus.Success
				};

			}
			catch (Exception ex)
			{

				return new GetResponse<Article>
				{
					Status = ResponseStatus.Exception,
					ResponseMessage = ex.Message
				};
			}
		}

		public GetResponse<Article> GetById(Guid id)
		{
			try
			{
				var article = _dbContext.Articles.Find(id);

				return new GetResponse<Article>
				{
					Model = article,
					Status = ResponseStatus.Success
				};
			}
			catch (Exception e)
			{

				return new GetResponse<Article>
				{
					Status = ResponseStatus.Exception,
					ResponseMessage = e.Message
				};
			}
		}

		public GetResponse<ArticlePublicViewModel> GetDetailArticle(Guid id)
		{
			try
			{
				var article = _dbContext.Articles.Find(id);
				ArticlePublicViewModel ac = new ArticlePublicViewModel();
				return new GetResponse<ArticlePublicViewModel>
				{
					Model = ac,
					Status = ResponseStatus.Success
				};
			}
			catch (Exception e)
			{

				return new GetResponse<ArticlePublicViewModel>
				{
					Status = ResponseStatus.Exception,
					ResponseMessage = e.Message
				};
			}
		}
	}
	public interface IArticleService
	{
		UpdateResponse CreateOrUpdate(UpdateRequest<ArticleViewModel> request);
		int Count();
		GetResponse<Article> GetById(Guid id);
		GetResponse<ArticlePublicViewModel> GetDetailArticle(Guid id);
		GetResponse<Article> GetArticleForCategory(Guid cateid);
		GetResponse<Article> GetAll();
		GetResponse<Article> GetAllShow();
		DataTableJsResponse<Article> GetAll(DataTableJsRequest<Article> request, string q = null);

		PagedViewModel<ArticleViewModel> GetAllPaging(string key, int pageIndex, int pageSize);

		DeleteResponse Delete(Guid id);
	}


}