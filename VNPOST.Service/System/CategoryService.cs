﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using TinTuc.Data.EF;
using TinTuc.Data.Entities;
using TinTuc.Data.Entities.System;
using TinTuc.Data.Enums;
using TinTuc.Data.Infrastructure;
using TinTuc.Service.Models;
using TinTuc.Service.Models.Request;
using TinTuc.Service.Models.Response;
using TinTuc.Service.ViewModels;

namespace TinTuc.Service.System
{
	public class CategoryService : ICategoryService
	{
		private readonly TinTucDbContext _dbContext;
		private readonly Guid UserId = new Guid("b522dba4-e9dc-4a0e-baa8-dacf6477dab9");
		public CategoryService(TinTucDbContext dbContext)
		{
			_dbContext = dbContext;

		}
		public int Count()
		{
			try
			{
				return _dbContext.Categories.Count();
			}
			catch (Exception)
			{

				throw;
			}
		}

		public UpdateResponse CreateOrUpdate(UpdateRequest<Category> request)
		{
			try
			{
				//b0de8441-5c41-4570-89c0-67bbda218f53
				Guid CategoryIdNew = Guid.NewGuid();
				var Category = _dbContext.Categories.Find(request.Model.Id);
				if (Category != null)
				{
					Category.Name = request.Model.Name?.Trim();
					Category.Description = request.Model.Description == null ? "" : request.Model.Description.Trim();
					Category.ModifiedAt = DateTime.Now;
					Category.ParentId = request.Model.ParentId;
					Category.ParentName = GetNameCategoryById(request.Model.ParentId ?? Guid.Empty);
					Category.Status = request.Model.Status;
					Category.ModifiedBy = UserId.ToString();
					_dbContext.Categories.Attach(Category);
					_dbContext.SaveChanges();
				}
				else
				{
					var _category = new Category();
					_category.Id = CategoryIdNew;
					_category.Name = request.Model.Name;
					_category.Description = request.Model.Description == null ?  "" : request.Model.Description.Trim();
					_category.CreatedAt = DateTime.Now;
					_category.CreatedBy = UserId.ToString();
					_category.ParentId = request.Model.ParentId;
					_category.ParentName = GetNameCategoryById(request.Model.ParentId ?? Guid.Empty);
					_category.UserId = UserId;
					_category.Status = request.Model.Status;
					_dbContext.Categories.Add(_category);
					_dbContext.SaveChanges();

				}
				return new UpdateResponse
				{
					Id = request.Model.Id,
					ResponseMessage = "Thành công",
					Status = ResponseStatus.Success
				};
				//var Category = _dbContext.Categories.Find(request.Model.Id);
				//request.Model.Name = request.Model.Name?.Trim();
				//var cate = _dbContext.Categories.Find(request.Model.Id);
				//request.Model.Name = request.Model.Name.Trim();
			}
			catch (Exception ex)
			{
				return new UpdateResponse
				{
					Status = ResponseStatus.Exception,
					ResponseMessage = ex.Message
				};

			}
			throw new NotImplementedException();
		}

		public DeleteResponse Delete(Guid id)
		{
			try
			{
				var Cate = _dbContext.Categories.Find(id);
				var findListChildCate = _dbContext.Categories.Where(x => x.ParentId == Cate.Id).ToList();
				foreach (var item in findListChildCate)
				{
					Delete(item.Id);
				}
				_dbContext.Categories.Attach(Cate);
				_dbContext.Categories.Remove(Cate);
				_dbContext.SaveChanges();
				return new DeleteResponse
				{
					Status = ResponseStatus.Success,
					ResponseMessage = "Thành công"
				};
			}
			catch (Exception ex)
			{
				return new DeleteResponse
				{
					Status = ResponseStatus.Exception,
					ResponseMessage = ex.Message
				};
			}
		}

		public GetResponse<Category> GetAllCateShow()
		{
			try
			{
				var listCategory = _dbContext.Categories
								.Include(c => c.User)
								.Where(x => x.Status == Status.Active)
								.ToList();
				foreach (var item in listCategory)
				{
					if (item.ParentId != null)
					{
						item.ParentName = GetNameCategoryById(item.ParentId ?? Guid.Empty);
					}
				}
				return new GetResponse<Category>
				{
					Models = listCategory.ToList().OrderByDescending(e => e.CreatedAt),
					Status = ResponseStatus.Success
				};
			}
			catch (Exception e)
			{
				return new GetResponse<Category>
				{
					Status = ResponseStatus.Exception,
					ResponseMessage = e.Message
				};
			}
		}

		public DataTableJsResponse<Category> GetAll(DataTableJsRequest<Category> request, string q = null)
		{   /*Something*/
			throw new NotImplementedException();
		}

		public GetResponse<Category> GetAllCategory()
		{
			try
			{
				var listCategory = _dbContext.Categories
								.Include(c => c.User)
								.ToList();
				foreach (var item in listCategory)
				{
					if (item.ParentId != null)
					{
						item.ParentName = GetNameCategoryById(item.ParentId ?? Guid.Empty);
					}
				}
				//foreach (var item in listCategory)
				//{
				//	CategoryViewModel cate = new CategoryViewModel();
				//	cate.Id = item.Id;
				//	cate.Name = item.Name;
				//	cate.User.FullName = item.User.FullName;
				//}
				return new GetResponse<Category>
				{
					Models = listCategory.ToList().OrderByDescending(e => e.CreatedAt),
					Status = ResponseStatus.Success
				};
			}
			catch (Exception e)
			{
				return new GetResponse<Category>
				{
					Status = ResponseStatus.Exception,
					ResponseMessage = e.Message
				};
			}
		}
		public string GetNameCategoryById(Guid id)
		{
			try
			{
				return _dbContext.Categories.Where(x => x.Id == id).FirstOrDefault().Name.ToString();
			}
			catch (Exception)
			{

				return null;
			}
		}
		public PagedViewModel<CategoryViewModel> GetAllPaging(string key, int pageIndex, int pageSize)
		{   /*Something*/
			throw new NotImplementedException();
		}

		public GetResponse<Category> GetById(Guid id)
		{
			try
			{
				var category = _dbContext.Categories.Find(id);
				if (category.ParentId != null)
				{
					var cateName = _dbContext.Categories.Where(x => x.Id == category.ParentId).FirstOrDefault().Name.ToString();
					category.ParentName = cateName;

				}
				return new GetResponse<Category>
				{
					Model = category,
					Status = ResponseStatus.Success
				};
			}
			catch (Exception e)
			{

				return new GetResponse<Category>
				{
					Status = ResponseStatus.Exception,
					ResponseMessage = e.Message
				};
			}
		}


		public List<Category> GetChildById(Guid id)
		{
			var lstCate = _dbContext.Categories.Where(a => a.ParentId.Equals(id)).OrderBy(x => x.CreatedAt).ToList();
			return lstCate;
		}

		public List<Category> GetListCategoryHeader()
		{
			var list = _dbContext.Categories.Where(lst => lst.CreatedBy.Length <= 2).OrderBy(lst => lst.CreatedBy).ToList();
			return list;
		}
	}
	public interface ICategoryService
	{
		UpdateResponse CreateOrUpdate(UpdateRequest<Category> request);
		int Count();
		GetResponse<Category> GetAllCateShow();
		GetResponse<Category> GetAllCategory();
		GetResponse<Category> GetById(Guid id);
		List<Category> GetChildById(Guid id);
		List<Category> GetListCategoryHeader();
		DataTableJsResponse<Category> GetAll(DataTableJsRequest<Category> request, string q = null);
		PagedViewModel<CategoryViewModel> GetAllPaging(string key, int pageIndex, int pageSize);
		DeleteResponse Delete(Guid id);


	}
}
