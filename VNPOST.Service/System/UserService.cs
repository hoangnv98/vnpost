﻿using Common;
using Microsoft.AspNetCore.Identity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using TinTuc.Data.EF;
using TinTuc.Data.Entities.System;
using TinTuc.Service.Models;
using TinTuc.Service.Models.Request;
using TinTuc.Service.Models.Response;

namespace TinTuc.Service.System
{
	public class UserService : IUserService
	{
		private readonly TinTucDbContext _dbContext;
		public UserService(TinTucDbContext dbContext)
		{
			_dbContext = dbContext;
		}

		public int Count()
		{
			try
			{
				int count = _dbContext.Users.Count();
				return count;
			}
			catch (Exception)
			{

				throw;
			}
		}

		public UpdateResponse CreateOrUpdate(UpdateRequest<User> request)
		{
			try
			{
				var user = _dbContext.Users.Find(request.Model.Id);
				var hasher = new PasswordHasher<User>();
				if (user != null)
				{
					user.FullName = request.Model.FullName?.Trim();
					user.Email = request.Model.Email;
					user.UserName = request.Model.UserName;
					user.Gender = request.Model.Gender;
					user.PhoneNumber = request.Model.PhoneNumber;
					user.PasswordHash = hasher.HashPassword(null, request.Model.PasswordHash);
					user.LockoutEnabled = request.Model.LockoutEnabled;
					_dbContext.Users.Attach(user);
					_dbContext.Users.Update(user);
					_dbContext.SaveChanges();
				}
				else
				{
					var _user = new User();
					_user.Id = Guid.NewGuid();
					_user.FullName = request.Model.FullName?.Trim();
					_user.Email = request.Model.Email;
					_user.UserName = request.Model.UserName;
					_user.Gender = request.Model.Gender;
					_user.PasswordHash = hasher.HashPassword(null, request.Model.PasswordHash);
					_user.PhoneNumber = request.Model.PhoneNumber;
					_user.LockoutEnabled = request.Model.LockoutEnabled;
					_dbContext.Users.Add(_user);
					_dbContext.SaveChanges();

				}
				return new UpdateResponse
				{
					Id = request.Model.Id,
					ResponseMessage = "Thành công",
					Status = ResponseStatus.Success
				};
			}
			catch (Exception e)
			{
				return new UpdateResponse
				{
					Status = ResponseStatus.Exception,
					ResponseMessage = e.Message
				};
			}
		}


		public GetResponse<User> GetAll()
		{
			try
			{
				var listUser = _dbContext.Users.ToList();
				return new GetResponse<User>
				{
					Models = listUser.OrderByDescending(e => e.CreatedAt),
					Status = ResponseStatus.Success
				};
			}
			catch (Exception e)
			{
				return new GetResponse<User>
				{
					Status = ResponseStatus.Exception,
					ResponseMessage = e.Message
				};
			}
		}

		public GetResponse<User> GetById(Guid id)
		{
			try
			{
				var user = _dbContext.Users.Find(id);
				
				return new GetResponse<User>
				{
					Model = user,
					Status = ResponseStatus.Success
				};
			}
			catch (Exception e)
			{

				return new GetResponse<User>
				{
					Status = ResponseStatus.Exception,
					ResponseMessage = e.Message
				};
			}
		}

		public User GetUserByUserName(string username)
		{
			try
			{
				var user = _dbContext.Users.SingleOrDefault(x => x.UserName == username);
				return user;
			}
			catch (Exception)
			{

				throw;
			}
		}

		public User Login(string userName, string password)
		{

			try
			{
				var user = _dbContext.Users.Where(x => x.UserName == userName).FirstOrDefault();
				if (user != null)
				{
					if (EncryptHelper.VerifyPwd(user.PasswordHash, password))
					{

						// Update last login
						user.LastLogin = DateTime.Now;
						_dbContext.Users.Update(user);
						_dbContext.SaveChanges();
						return user;
					}
				}

				return null;
			}
			catch (Exception e)
			{
				return null;
			}
		}
	}
	public interface IUserService
	{
		int Count();
		User Login(string userName, string password);
		User GetUserByUserName(string username);
		GetResponse<User> GetAll();
		UpdateResponse CreateOrUpdate(UpdateRequest<User> request);
		GetResponse<User> GetById(Guid id);

	}
}
