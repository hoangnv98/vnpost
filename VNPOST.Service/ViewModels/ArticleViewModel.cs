﻿using System;
using System.Collections.Generic;
using System.Text;
using TinTuc.Data.Enums;

namespace TinTuc.Service.ViewModels
{
	public class ArticleViewModel
	{
		public Guid Id { get; set; }
		public string Title { get; set; }
		public string Thumbnail { get; set; }
		public string Content { get; set; }
		public Status Status { get; set; }
		public List<Guid> ListCategory { get; set; }
	}
}
