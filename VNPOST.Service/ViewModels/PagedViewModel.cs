﻿using System;
using System.Collections.Generic;
using System.Text;

namespace TinTuc.Service.ViewModels
{
	public class PagedViewModel<T>
	{
		public List<T> Models { get; set; }
		public int TotalRecord { get; set; }
	}
}
