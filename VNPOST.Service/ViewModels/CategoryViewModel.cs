﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;
using TinTuc.Data.Entities.System;
using TinTuc.Data.Enums;

namespace TinTuc.Service.ViewModels
{
	public class CategoryViewModel
	{
		public Guid? Id { get; set; }
		public string  Name { get; set; }
	}
}
