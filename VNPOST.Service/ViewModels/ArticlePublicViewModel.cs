﻿using System;
using System.Collections.Generic;
using System.Text;
using TinTuc.Data.Entities;
using TinTuc.Data.Entities.System;
using TinTuc.Data.Enums;

namespace TinTuc.Service.ViewModels
{
	public class ArticlePublicViewModel
	{
		public Guid Id { get; set; }
		public string Title { get; set; }
		public string Thumbnail { get; set; }
		public string Content { get; set; }
		public DateTime? CreateAt { get; set; }
		public Status Status { get; set; }
		public User User { get; set; }
		public Category Category { get; set; }

	}
	public class ArticlePublicViewModelTemp
	{
		public Guid Id { get; set; }
		public string Title { get; set; }
		public string Thumbnail { get; set; }
		public string Content { get; set; }
		public DateTime? CreateAt { get; set; }
		public Status Status { get; set; }
		public string UserName { get; set; }
		public string CategoryName { get; set; }
		public Guid CategoryId { get; set; }
		//public User User { get; set; }
		//public Category Category { get; set; }

	}
}
