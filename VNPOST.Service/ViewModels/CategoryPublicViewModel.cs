﻿using System;
using System.Collections.Generic;
using System.Text;
using TinTuc.Data.Entities;

namespace TinTuc.Service.ViewModels
{
	public class CategoryPublicViewModel
	{
		public Guid Id { get; set; }
		public string  Name { get; set; }
		public DateTime? CreatedAt { get; set; }
		public List<Category>?  ListCategoryChild { get; set; }
	}
}
