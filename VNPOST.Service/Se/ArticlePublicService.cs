﻿using Common;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using TinTuc.Data.EF;
using TinTuc.Data.Entities;
using TinTuc.Data.Enums;
using TinTuc.Service.Models;
using TinTuc.Service.Models.Response;
using TinTuc.Service.ViewModels;

namespace TinTuc.Service.Se
{
	public class ArticlePublicService : IArticlePublicService
	{
		private readonly TinTucDbContext _dbContext;
		public ArticlePublicService(TinTucDbContext dbContext)
		{
			_dbContext = dbContext;
		}
		public int Count()
		{
			return _dbContext.Articles.Count();
		}
		public void MapArticle(ArticlePublicViewModel article, Article a)
		{
			article.Id = a.Id;
			article.Title = a.Title;
			article.Thumbnail = a.Thumbnail;
			article.Status = a.Status;
			article.User = a.User;
			article.Content = a.Content;
			article.CreateAt = a.CreatedAt;
		}
		public GetResponse<ArticlePublicViewModel> GetAll()
		{
			try
			{
				var list = _dbContext.Articles.Where(x => x.Status == Status.Active)
					.Include(e => e.User)
					.ToList();

				List<ArticlePublicViewModel> lstArticle = new List<ArticlePublicViewModel>();
				foreach (var item in list)
				{
					var idCateFirst = _dbContext.ArticleCategories.FirstOrDefault(x => x.ArticleId == item.Id).CategoryId;
					var cate = _dbContext.Categories.Find(idCateFirst);
					ArticlePublicViewModel article = new ArticlePublicViewModel();
					MapArticle(article, item);
					article.Category = cate;
					lstArticle.Add(article);
				}
				//foreach (var item in list)
				//{
				//	var x = _dbContext.ArticleCategories.Select(ac => ac.ArticleId == item.Id).ToList();
				//	//item.ArticleCategories = _dbContext.ArticleCategories.Where(ac => ac.ArticleId == ""));
				//}
				return new GetResponse<ArticlePublicViewModel>
				{
					Models = lstArticle.OrderByDescending(e => e.CreateAt),
					Status = ResponseStatus.Success
				};


			}
			catch (Exception e)
			{

				return new GetResponse<ArticlePublicViewModel>
				{
					Status = ResponseStatus.Exception,
					ResponseMessage = e.Message
				};
			}
		}

		public GetResponse<ArticlePublicViewModelTemp> GetAllTemp()
		{
			try
			{
				var lstArticle = _dbContext.Articles.Where(x => x.Status == Status.Active)
					.Include(e => e.User).ToList()
					.Select(e =>
					{
						var idCateFirst = _dbContext.ArticleCategories.FirstOrDefault(x => x.ArticleId == e.Id).CategoryId;
						var cate = _dbContext.Categories.Find(idCateFirst);
						var temp = new ArticlePublicViewModelTemp
						{
							Id = e.Id,
							Content = ConvertHelper.StripHTML(e.Content),
							UserName = e.User.UserName,
							Title = e.Title,
							Thumbnail = e.Thumbnail,
							Status = e.Status,
							CreateAt = e.CreatedAt,
							CategoryName = cate.Name,
							CategoryId = cate.Id
						};
						return temp;
					});

				return new GetResponse<ArticlePublicViewModelTemp>
				{
					Models = lstArticle.OrderByDescending(e => e.CreateAt),
					Status = ResponseStatus.Success
				};
			}
			catch (Exception e)
			{

				return new GetResponse<ArticlePublicViewModelTemp>
				{
					Status = ResponseStatus.Exception,
					ResponseMessage = e.Message
				};
			}
		}

		public GetResponse<Article> GetArticleForCategory(Guid cateid)
		{
			try
			{
				var listArticle = _dbContext.ArticleCategories.Where(x => x.CategoryId == cateid).ToList();
				List<Article> lstArticle = new List<Article>();

				foreach (var item in listArticle)
				{
					var article = _dbContext.Articles.Where(x => x.Id == item.ArticleId).FirstOrDefault();
					var user = _dbContext.Users.Find(article.UserId);
					lstArticle.Add(article);
				}
				return new GetResponse<Article>
				{
					Models = lstArticle,
					Status = ResponseStatus.Success
				};

			}
			catch (Exception ex)
			{

				return new GetResponse<Article>
				{
					Status = ResponseStatus.Exception,
					ResponseMessage = ex.Message
				};
			}
		}

		public GetResponse<ArticlePublicViewModel> GetById(Guid id)
		{
			try
			{
				var article = _dbContext.Articles.Find(id);
				var user = _dbContext.Users.Find(article.UserId);
				ArticlePublicViewModel articleViewModel = new ArticlePublicViewModel();
				if (article != null)
				{
					var acId = _dbContext.ArticleCategories.FirstOrDefault(x => x.ArticleId == id).CategoryId;
					var category = _dbContext.Categories.Find(acId);
					MapArticle(articleViewModel, article);
					articleViewModel.Id = article.Id;
					articleViewModel.Category = category;
					articleViewModel.User = user;
				}
				return new GetResponse<ArticlePublicViewModel>
				{
					Model = articleViewModel,
					Status = ResponseStatus.Success
				};
			}
			catch (Exception e)
			{

				return new GetResponse<ArticlePublicViewModel>
				{
					Status = ResponseStatus.Exception,
					ResponseMessage = e.Message
				};
			}
		}

		public GetResponse<ArticlePublicViewModel> GetAllRandom()
		{
			try
			{
				var list = _dbContext.Articles.Where(x => x.Status == Status.Active)
					.Include(c => c.User)
					.ToList();

				List<ArticlePublicViewModel> lstArticle = new List<ArticlePublicViewModel>();
				foreach (var item in list)
				{
					var idCateFirst = _dbContext.ArticleCategories.FirstOrDefault(x => x.ArticleId == item.Id).CategoryId;
					var cate = _dbContext.Categories.Find(idCateFirst);
					ArticlePublicViewModel article = new ArticlePublicViewModel();
					MapArticle(article, item);
					article.Category = cate;
					lstArticle.Add(article);
				}
				//foreach (var item in list)
				//{
				//	var x = _dbContext.ArticleCategories.Select(ac => ac.ArticleId == item.Id).ToList();
				//	//item.ArticleCategories = _dbContext.ArticleCategories.Where(ac => ac.ArticleId == ""));
				//}
				return new GetResponse<ArticlePublicViewModel>
				{
					Models = lstArticle.OrderBy(x => Guid.NewGuid()),
					Status = ResponseStatus.Success
				};
			}
			catch (Exception e)
			{

				return new GetResponse<ArticlePublicViewModel>
				{
					Status = ResponseStatus.Exception,
					ResponseMessage = e.Message
				};
			}
		}

		public GetResponse<ArticlePublicViewModel> GetArticleViewModelForCategory(Guid cateid)
		{
			try
			{
				var listArticle = _dbContext.ArticleCategories.Where(x => x.CategoryId == cateid).ToList();
				List<ArticlePublicViewModel> lstArticle = new List<ArticlePublicViewModel>();

				foreach (var item in listArticle)
				{
					var article = _dbContext.Articles.Where(x => x.Id == item.ArticleId).FirstOrDefault();
					var user = _dbContext.Users.Find(article.UserId);
					ArticlePublicViewModel acvm = new ArticlePublicViewModel();
					MapArticle(acvm, article);
					acvm.Category = _dbContext.Categories.Find(cateid);
					acvm.User = user;
					lstArticle.Add(acvm);
				}
				return new GetResponse<ArticlePublicViewModel>
				{
					Models = lstArticle,
					Status = ResponseStatus.Success
				};

			}
			catch (Exception ex)
			{

				return new GetResponse<ArticlePublicViewModel>
				{
					Status = ResponseStatus.Exception,
					ResponseMessage = ex.Message
				};
			}
		}

		public GetResponse<ArticlePublicViewModelTemp> GetWithNumber(int number)
		{

			try
			{
				var lstArticle = _dbContext.Articles.Where(x => x.Status == Status.Active)
					.Include(e => e.User).ToList()
					.Select(e =>
					{
						var idCateFirst = _dbContext.ArticleCategories.FirstOrDefault(x => x.ArticleId == e.Id).CategoryId;
						var cate = _dbContext.Categories.Find(idCateFirst);
						var temp = new ArticlePublicViewModelTemp
						{
							Id = e.Id,
							Content = ConvertHelper.StripHTML(e.Content),
							UserName = e.User.UserName,
							Title = e.Title,
							Thumbnail = e.Thumbnail,
							Status = e.Status,
							CreateAt = e.CreatedAt,
							CategoryName = cate.Name,
							CategoryId = cate.Id
						};
						return temp;
					}).Take(number);

				return new GetResponse<ArticlePublicViewModelTemp>
				{
					Models = lstArticle.OrderByDescending(e => e.CreateAt),
					Status = ResponseStatus.Success
				};
			}
			catch (Exception e)
			{

				return new GetResponse<ArticlePublicViewModelTemp>
				{
					Status = ResponseStatus.Exception,
					ResponseMessage = e.Message
				};
			}

		}

		public GetResponse<ArticlePublicViewModelTemp> GetArticleById(Guid id)
		{
			try
			{
				var Article = _dbContext.Articles.Where(x => x.Status == Status.Active && x.Id == id)
					.Include(e => e.User).ToList()
					.Select(e =>
					{
						var idCateFirst = _dbContext.ArticleCategories.FirstOrDefault(x => x.ArticleId == e.Id).CategoryId;
						var cate = _dbContext.Categories.Find(idCateFirst);
						var temp = new ArticlePublicViewModelTemp
						{
							Id = e.Id,
							Content = e.Content,
							UserName = e.User.UserName,
							Title = e.Title,
							Thumbnail = e.Thumbnail,
							Status = e.Status,
							CreateAt = e.CreatedAt,
							CategoryName = cate.Name,
							CategoryId = cate.Id
						};
						return temp;
					}).FirstOrDefault();

				return new GetResponse<ArticlePublicViewModelTemp>
				{
					Model = Article,
					Status = ResponseStatus.Success
				};
			}
			catch (Exception e)
			{

				return new GetResponse<ArticlePublicViewModelTemp>
				{
					Status = ResponseStatus.Exception,
					ResponseMessage = e.Message
				};
			}
		}
	}
	public interface IArticlePublicService
	{
		int Count();
		GetResponse<ArticlePublicViewModel> GetById(Guid id);
		GetResponse<ArticlePublicViewModelTemp> GetArticleById(Guid id);

		GetResponse<Article> GetArticleForCategory(Guid cateid);
		GetResponse<ArticlePublicViewModel> GetArticleViewModelForCategory(Guid cateid);
		GetResponse<ArticlePublicViewModel> GetAll();
		GetResponse<ArticlePublicViewModel> GetAllRandom();

		GetResponse<ArticlePublicViewModelTemp> GetAllTemp();
		GetResponse<ArticlePublicViewModelTemp> GetWithNumber(int number);

	}
}
