﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using TinTuc.Service.ViewModels;
using TinTuc.Data.EF;
using TinTuc.Data.Entities;
using TinTuc.Data.Enums;
using TinTuc.Service.Models;
using TinTuc.Service.Models.Response;

namespace TinTuc.Service.Se
{
	public class CategoryPublicService : ICategoryPublicService
	{
		private readonly TinTucDbContext _dbContext;
		public CategoryPublicService(TinTucDbContext dbContext)
		{
			_dbContext = dbContext;
		}
		public int Count()
		{
			return _dbContext.Categories.Count();
		}
		public void MapArticle(ArticlePublicViewModel article, Article a)
		{
			article.Id = a.Id;
			article.Title = a.Title;
			article.Thumbnail = a.Thumbnail;
			article.Status = a.Status;
			article.User = a.User;
			article.Content = a.Content;
			article.CreateAt = a.CreatedAt;
		}
		public GetResponse<CategoryPublicViewModel> GetAll()
		{
			var listCategory = _dbContext.Categories.Where(x => x.Status == Status.Active).Include(x => x.User)
								.ToList();
			List<CategoryPublicViewModel> lstCateViewModel = new List<CategoryPublicViewModel>();
			foreach (var item in listCategory)
			{
				if (item.ParentId == null)
				{
					CategoryPublicViewModel cateViewModel = new CategoryPublicViewModel();
					cateViewModel.Id = item.Id;
					cateViewModel.Name = item.Name;
					cateViewModel.CreatedAt = item.CreatedAt;
					cateViewModel.ListCategoryChild = _dbContext.Categories.Where(x => x.ParentId == item.Id && x.Status == Status.Active).ToList();
					lstCateViewModel.Add(cateViewModel);
				}
			}

			return new GetResponse<CategoryPublicViewModel>
			{
				Models = lstCateViewModel.ToList().OrderByDescending(e => e.CreatedAt),
				Status = ResponseStatus.Success
			};
		}

		public GetResponse<ArticlePublicViewModel> GetArticleById(Guid cateid)
		{
			try
			{
				var listArticle = _dbContext.ArticleCategories.Where(x => x.CategoryId == cateid).ToList();
				List<ArticlePublicViewModel> lstArticle = new List<ArticlePublicViewModel>();

				foreach (var item in listArticle)
				{
					var article = _dbContext.Articles.Where(x => x.Id == item.ArticleId).FirstOrDefault();
					var user = _dbContext.Users.Find(article.UserId);
					ArticlePublicViewModel acvm = new ArticlePublicViewModel();
					MapArticle(acvm, article);
					acvm.Category = _dbContext.Categories.Find(cateid);
					acvm.User = user;
					lstArticle.Add(acvm);
				}
				return new GetResponse<ArticlePublicViewModel>
				{
					Models = lstArticle,
					Status = ResponseStatus.Success
				};

			}
			catch (Exception ex)
			{

				return new GetResponse<ArticlePublicViewModel>
				{
					Status = ResponseStatus.Exception,
					ResponseMessage = ex.Message
				};
			}
		}

		public GetResponse<CategoryPublicViewModel> GetAllCategory()
		{
			var listCategory = _dbContext.Categories.Where(x => x.Status == Status.Active).Include(x => x.User)
								.ToList();
			List<CategoryPublicViewModel> lstCateViewModel = new List<CategoryPublicViewModel>();
			foreach (var item in listCategory)
			{
				if (item.ParentId == null)
				{
					CategoryPublicViewModel cateViewModel = new CategoryPublicViewModel();
					cateViewModel.Id = item.Id;
					cateViewModel.Name = item.Name;
					cateViewModel.CreatedAt = item.CreatedAt;
					//cateViewModel.ListCategoryChild = _dbContext.Categories.Where(x => x.ParentId == item.Id && x.Status == Status.Active).ToList();
					lstCateViewModel.Add(cateViewModel);
				}
			}

			return new GetResponse<CategoryPublicViewModel>
			{
				Models = lstCateViewModel.ToList().OrderByDescending(e => e.CreatedAt),
				Status = ResponseStatus.Success
			};
		}
	}
	public interface ICategoryPublicService
	{
		int Count();
		GetResponse<CategoryPublicViewModel> GetAll();
		GetResponse<CategoryPublicViewModel> GetAllCategory();
		GetResponse<ArticlePublicViewModel> GetArticleById(Guid id);
		

	}
}
