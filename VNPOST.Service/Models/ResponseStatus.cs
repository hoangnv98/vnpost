﻿using System;
using System.Collections.Generic;
using System.Text;

namespace TinTuc.Service.Models
{
    public enum ResponseStatus
    {
        Success = 0,
        Warning = 1,
        Fail = 2,
        Exception = 3,
        Unauthorized = 4
    }
}
