﻿using System;
using System.Collections.Generic;
using System.Text;

namespace TinTuc.Service.Models.Response
{
    public class GetResponse<T> : ResponseBase
    {
        public T Model { get; set; }
        public IEnumerable<T> Models { get; set; }
    }
}
