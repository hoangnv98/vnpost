﻿using System;
using System.Collections.Generic;
using System.Text;

namespace TinTuc.Service.Models.Response
{
    public class CreateResponse : ResponseBase
    {
        public Guid Id { get; set; }
        public IEnumerable<ValidateError> Errors { get; set; }
    }
}
