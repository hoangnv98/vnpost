﻿using System;
using System.Collections.Generic;
using System.Text;

namespace TinTuc.Service.Models.Response
{
    public class DeleteResponse : ResponseBase
    {
        public Guid Id { get; set; }
    }
}
