﻿using System;
using System.Collections.Generic;
using System.Text;

namespace TinTuc.Service.Models.Response
{
    public class AuthResponse
    {
        public string UserName { get; set; }
        public string FullName { get; set; }
        public string Message { get; set; }
        public ResponseStatus Status { get; set; }
    }
}
