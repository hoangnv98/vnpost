﻿using System;
using System.Collections.Generic;
using System.Text;

namespace TinTuc.Service.Models.Response
{
	public class ResponseBase
	{
		public string ResponseMessage { get; set; }

		public ResponseStatus Status { get; set; }
	}
}
