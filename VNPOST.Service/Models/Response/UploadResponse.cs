﻿using System;
using System.Collections.Generic;
using System.Text;

namespace TinTuc.Service.Models.Response
{
    public class UploadResponse : ResponseBase
    {
        public string Path { get; set; }
    }
}
