﻿using System;
using System.Collections.Generic;
using System.Text;

namespace TinTuc.Service.Models.Response
{
    public class DataTableJsResponse<T>
    {
        public DataTableJsResponse()
        {
            Data = new List<T>();
            Draw = 0;
            RecordsFiltered = 0;
            PageIndex = 0;
            TotalPage = 0;
        }

        public IEnumerable<T> Data { get; set; }
        public int Draw { get; set; }
        public int RecordsFiltered { get; set; }
        public int RecordsTotal { get; set; }
        public int PageIndex { get; set; }
        public int TotalPage { get; set; }
    }
}
