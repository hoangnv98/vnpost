﻿using System;
using System.Collections.Generic;
using System.Text;

namespace TinTuc.Service.Models
{
	public class ValidateError
	{
		public string Field { get; }
		public string Message { get; }

		public ValidateError(string field, string message)
		{
			Field = field != string.Empty ? field : null;
			Message = message;
		}
	}
}
