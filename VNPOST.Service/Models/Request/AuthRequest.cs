﻿using System;
using System.Collections.Generic;
using System.Text;

namespace TinTuc.Service.Models.Request
{
	public class AuthRequest
	{
		public string UserName { get; set; }

		public string Password { get; set; }
		public bool Remember { get; set; }
		public class UpdatePasswordRequest
		{
			public string OldPassword { get; set; }
			public string NewPassword { get; set; }
		}
	}
}
