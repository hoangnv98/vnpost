﻿using System;
using System.Collections.Generic;
using System.Text;

namespace TinTuc.Service.Models.Request
{
    public class DataTableJsRequest<T>
    {
        public int Start { get; set; }
        public int Length { get; set; }
        public int Draw { get; set; }
        public T Filter { get; set; }

        public DataTableJsRequest()
        {
            Start = 0;
            Length = 10;
        }
    }
}
