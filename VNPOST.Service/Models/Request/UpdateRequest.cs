﻿using System;
using System.Collections.Generic;
using System.Text;

namespace TinTuc.Service.Models.Request
{
    public class UpdateRequest<T> : RequestBase
    {
        public T Model { get; set; }
    }
}
