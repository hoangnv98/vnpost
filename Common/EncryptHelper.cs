﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Common
{
    public class EncryptHelper
    {
        public static string EncryptPwd(string pwd)
        {
            return BCrypt.Net.BCrypt.HashPassword(pwd);
        }

        public static bool VerifyPwd(string hashPwd, string pwd)
        {
            return BCrypt.Net.BCrypt.Verify(pwd, hashPwd);
        }
    }
}
